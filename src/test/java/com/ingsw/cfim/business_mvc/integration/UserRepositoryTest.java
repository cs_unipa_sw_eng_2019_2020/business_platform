package com.ingsw.cfim.business_mvc.integration;

import static org.junit.Assert.*;

import com.ingsw.cfim.business_mvc.common.configurations.MongoConfiguration;
import com.ingsw.cfim.business_mvc.common.models.Role;
import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.common.repositories.RoleRepository;
import com.ingsw.cfim.business_mvc.common.repositories.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@Import(MongoConfiguration.class)
public class UserRepositoryTest {
  @Autowired
  private UserRepository userRepo;

  @Autowired
  private RoleRepository roleRepo;

  @Before
  public void setup() {
    Role admin = new Role("admin");
    this.roleRepo.save(admin);

    User me = new User("fortest@example.org", "test");
    me.setEnabled(true);

    me.setRoles(this.roleRepo.findByRole("admin"));

    this.userRepo.save(me);

    User marco = new User("fortest2@gmail.com", "test");
    marco.setEnabled(false);
    this.userRepo.save(marco);
  }

  @Test
  public void testingExistanceOfAUser() {
    User found = this.userRepo.findUserByEmail("fortest@example.org");
    assertNotNull(found);
  }

  @Test
  public void testingNonExistanceOfAUser() {
    assertNull(this.userRepo.findUserByEmail("master@example.org"));
  }

  @Test(expected = DuplicateKeyException.class)
  public void userWithSameEmailNotAllowed() {
    this.userRepo.save(new User("fortest@example.org", "test"));
  }

  @Test
  public void myUserIsEnabled() {
    assertTrue(this.userRepo.findUserByEmail("fortest@example.org").getEnabled());
  }

  @Test
  public void marcoUserIsNotEnabled() {
    assertFalse(this.userRepo.findUserByEmail("fortest2@gmail.com").getEnabled());
  }

  @Test
  public void iHaveAdminRole() {
    Set<Role> roles = this.userRepo.findUserByEmail("fortest@example.org").getRoles();

    assertFalse(roles.isEmpty());

    Object[] result = roles.stream().map((Role role) -> role.getRole().equals("admin")).toArray();

    assertEquals(1, result.length);
    assertEquals(true, result[0]);
  }

  @Test
  public void marcoDoesntHaveRoles() {
    assertEquals(0, this.userRepo.findUserByEmail("fortest2@gmail.com").getRoles().size());
  }

  @After
  public void destroy() {
    this.userRepo.deleteAll();
    this.roleRepo.deleteAll();
  }
}

package com.ingsw.cfim.business_mvc.registration;

import static org.junit.Assert.*;

import com.ingsw.cfim.business_mvc.auth.PasswordService;
import com.ingsw.cfim.business_mvc.common.models.Role;
import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.common.repositories.RoleRepository;
import com.ingsw.cfim.business_mvc.common.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DuplicateKeyException;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationServiceTest {
  @Mock
  private PasswordService pwdSvc;

  @Mock
  private UserRepository userRepo;

  @Mock
  private RoleRepository roleRepo;

  @InjectMocks
  private RegistrationService regSvc;

  private User user;

  @Before
  public void setup() {
    this.user = new User("example@gmail.com", "test pwd hashed");
    user.setRoles(new Role("restaurant_owner"));

    Mockito.when(this.pwdSvc.encode(Mockito.anyString())).thenReturn("test pwd hashed");
  }

  @Test
  public void testRegistration() {
    Mockito.when(this.userRepo.findUserByEmail(Mockito.anyString())).thenReturn(null);

    Mockito.when(this.roleRepo.findByRole(Mockito.anyString())).thenReturn(new Role("restaurant_owner"));

    Mockito.when(this.userRepo.save(Mockito.any())).thenReturn(this.user);

    User newUser = new User("example@gmail.com", "test pwd");

    User registered = this.regSvc.register(newUser);
    assertNotNull(registered);

    assertEquals(this.user, registered);
  }

  @Test(expected = DuplicateKeyException.class)
  public void testRegistrationError() {
    Mockito.when(this.userRepo.findUserByEmail(Mockito.anyString())).thenReturn(this.user);

    User newUserForm = new User();

    newUserForm.setEmail("example@gmail.com");
    newUserForm.setPassword("test pwd");

    this.regSvc.register(newUserForm);
  }
}

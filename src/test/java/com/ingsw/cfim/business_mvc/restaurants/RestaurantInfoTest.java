package com.ingsw.cfim.business_mvc.restaurants;

import static org.junit.Assert.*;

import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.restaurants.models.Address;
import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import com.ingsw.cfim.business_mvc.restaurants.models.TimeTable;
import com.ingsw.cfim.business_mvc.restaurants.models.WorkingTime;
import com.ingsw.cfim.business_mvc.restaurants.responses.RestaurantInfo;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashSet;

public class RestaurantInfoTest {

  private RestaurantInfo info;
  private Restaurant restaurant;

  @Before
  public void setup() {
    User proprietary = new User();
    proprietary.setId(ObjectId.get());
    Address address = new Address();
    address.setLabel("Via Zeta Venti, 6, 90046, Villaciambra");
    restaurant = new Restaurant();
    restaurant.setId(ObjectId.get());
    restaurant.setName("Pizzeria Da Alessandro");
    restaurant.setDescription("Pizzeria migliore perché è a casa mia");
    restaurant.setType("pizzeria");
    restaurant.setCuisine("italiana");
    restaurant.setPhoneNumber("0916631205");
    restaurant.setVatNumber("123456789012");
    restaurant.setAddress(address);
    restaurant.setOwner(proprietary);
    restaurant.setFoodCategories(new HashSet<String>(Arrays.asList(
        "pizze", "antipasti", "bevande", "panini"
    )));

    WorkingTime monday = new WorkingTime();
    WorkingTime tuesday = new WorkingTime();
    WorkingTime wednesday = new WorkingTime();
    WorkingTime thursday = new WorkingTime();
    WorkingTime friday = new WorkingTime();
    WorkingTime saturday = new WorkingTime();
    WorkingTime sunday = new WorkingTime();

    monday.setFree(true);

    tuesday.setOpening("07:00");
    tuesday.setClosing("23:00");

    wednesday.setOpening("07:00");
    wednesday.setClosing("23:00");

    thursday.setOpening("07:00");
    thursday.setClosing("23:00");

    friday.setOpening("07:00");
    friday.setClosing("23:00");

    saturday.setOpening("07:00");
    saturday.setClosing("23:00");

    sunday.setOpening("07:00");
    sunday.setClosing("23:00");


    TimeTable timeSchedule = new TimeTable();
    timeSchedule.setMonday(monday);
    timeSchedule.setTuesday(tuesday);
    timeSchedule.setWednesday(wednesday);
    timeSchedule.setThursday(thursday);
    timeSchedule.setFriday(friday);
    timeSchedule.setSaturday(saturday);
    timeSchedule.setSunday(sunday);

    restaurant.setWeeklyOpeningSchedule(timeSchedule);
  }

  @Test
  public void testOpeningStatusMustReturnOpen() throws ParseException {
    String today = LocalDateTime.now().getDayOfWeek().toString();
    TimeTable schedule = this.restaurant.getWeeklyOpeningSchedule();

    switch (today) {
      case "MONDAY":
        schedule.getMonday().setFree(false);

        schedule.getMonday().setOpening(
            correct(LocalTime.now().getHour() + ":" + LocalTime.now().getMinute())
        );

        schedule.getMonday().setClosing(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );
        break;
      case "TUESDAY":
        schedule.getTuesday().setFree(false);

        schedule.getTuesday().setOpening(
            correct(LocalTime.now().getHour() + ":" + LocalTime.now().getMinute())
        );

        schedule.getTuesday().setClosing(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );
        break;
      case "WEDNESDAY":
        schedule.getWednesday().setFree(false);

        schedule.getWednesday().setOpening(
            correct(LocalTime.now().getHour() + ":" + LocalTime.now().getMinute())
        );

        schedule.getWednesday().setClosing(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );
        break;
      case "THURSDAY":
        schedule.getThursday().setFree(false);

        schedule.getThursday().setOpening(
            correct(LocalTime.now().getHour() + ":" + LocalTime.now().getMinute())
        );

        schedule.getThursday().setClosing(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );
        break;
      case "FRIDAY":
        schedule.getFriday().setFree(false);

        schedule.getFriday().setOpening(
            correct(LocalTime.now().getHour() + ":" + LocalTime.now().getMinute())
        );

        schedule.getFriday().setClosing(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );
        break;
      case "SATURDAY":
        schedule.getSaturday().setFree(false);

        schedule.getSaturday().setOpening(
            correct(LocalTime.now().getHour() + ":" + LocalTime.now().getMinute())
        );

        schedule.getSaturday().setClosing(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );
        break;
      case "SUNDAY":
        schedule.getSunday().setFree(false);

        schedule.getSunday().setOpening(
            correct(LocalTime.now().getHour() + ":" + LocalTime.now().getMinute())
        );
        schedule.getSunday().setClosing(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );
        break;
    }
    restaurant.setWeeklyOpeningSchedule(schedule);
    info = new RestaurantInfo(restaurant);

    assertEquals("open", info.getOpeningStatus());
  }

  @Test
  public void testOpeningStatusMustReturnDayOff() throws ParseException {
    String today = LocalDateTime.now().getDayOfWeek().toString();
    TimeTable schedule = this.restaurant.getWeeklyOpeningSchedule();

    switch (today) {
      case "MONDAY":
        schedule.getMonday().setFree(true);
        break;
      case "TUESDAY":
        schedule.getTuesday().setFree(true);
        break;
      case "WEDNESDAY":
        schedule.getWednesday().setFree(true);
        break;
      case "THURSDAY":
        schedule.getThursday().setFree(true);
        break;
      case "FRIDAY":
        schedule.getFriday().setFree(true);
        break;
      case "SATURDAY":
        schedule.getSaturday().setFree(true);
        break;
      case "SUNDAY":
        schedule.getSunday().setFree(true);
        break;
    }

    restaurant.setWeeklyOpeningSchedule(schedule);
    info = new RestaurantInfo(restaurant);
    assertEquals("dayOff", info.getOpeningStatus());
  }

  @Test
  public void testOpeningStatusMustReturnClosed() throws ParseException {
    String today = LocalDateTime.now().getDayOfWeek().toString();
    TimeTable schedule = this.restaurant.getWeeklyOpeningSchedule();

    switch (today) {
      case "MONDAY":
        schedule.getMonday().setFree(false);

        schedule.getMonday().setOpening(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );

        schedule.getMonday().setClosing(
            correct(LocalTime.now().plusMinutes(600).getHour() + ":" + LocalTime.now().plusMinutes(600).getMinute())
        );
        break;
      case "TUESDAY":
        schedule.getTuesday().setFree(false);

        schedule.getTuesday().setOpening(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );

        schedule.getTuesday().setClosing(
            correct(LocalTime.now().plusMinutes(600).getHour() + ":" + LocalTime.now().plusMinutes(600).getMinute())
        );
        break;
      case "WEDNESDAY":
        schedule.getWednesday().setFree(false);

        schedule.getWednesday().setOpening(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );

        schedule.getWednesday().setClosing(
            correct(LocalTime.now().plusMinutes(600).getHour() + ":" + LocalTime.now().plusMinutes(600).getMinute())
        );
        break;
      case "THURSDAY":
        schedule.getThursday().setFree(false);

        schedule.getThursday().setOpening(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );

        schedule.getThursday().setClosing(
            correct(LocalTime.now().plusMinutes(600).getHour() + ":" + LocalTime.now().plusMinutes(600).getMinute())
        );
        break;
      case "FRIDAY":
        schedule.getFriday().setFree(false);

        schedule.getFriday().setOpening(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );

        schedule.getFriday().setClosing(
            correct(LocalTime.now().plusMinutes(600).getHour() + ":" + LocalTime.now().plusMinutes(600).getMinute())
        );
        break;
      case "SATURDAY":
        schedule.getSaturday().setFree(false);

        schedule.getSaturday().setOpening(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );

        schedule.getSaturday().setClosing(
            correct(LocalTime.now().plusMinutes(600).getHour() + ":" + LocalTime.now().plusMinutes(600).getMinute())
        );
        break;
      case "SUNDAY":
        schedule.getSunday().setFree(false);

        schedule.getSunday().setOpening(
            correct(LocalTime.now().plusMinutes(240).getHour() + ":" + LocalTime.now().plusMinutes(240).getMinute())
        );
        schedule.getSunday().setClosing(
            correct(LocalTime.now().plusMinutes(600).getHour() + ":" + LocalTime.now().plusMinutes(600).getMinute())
        );
        break;
    }

    restaurant.setWeeklyOpeningSchedule(schedule);
    info = new RestaurantInfo(restaurant);
    assertEquals("closed", info.getOpeningStatus());
  }

  @Test
  public void testOpeningStatusMustReturnNULL() throws ParseException {
    restaurant.setWeeklyOpeningSchedule(null);
    info = new RestaurantInfo(restaurant);
    assertNull(info.getOpeningStatus());
  }

  private String correct(String toCheck) {
    if(toCheck.length() == 5) return toCheck;
    return "0" + toCheck;
  }
}

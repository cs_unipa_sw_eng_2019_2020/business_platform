package com.ingsw.cfim.business_mvc.restaurants;


import static org.junit.Assert.*;

import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.restaurants.dao.RestaurantDao;
import com.ingsw.cfim.business_mvc.restaurants.models.Address;
import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import com.ingsw.cfim.business_mvc.restaurants.models.TimeTable;
import com.ingsw.cfim.business_mvc.restaurants.models.WorkingTime;
import com.ingsw.cfim.business_mvc.restaurants.repositories.RestaurantRepository;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;
import java.util.stream.Collectors;

@RunWith(MockitoJUnitRunner.class)
public class RestaurantDaoTest {
  HashMap<ObjectId, Restaurant> db = new HashMap<>();

  @InjectMocks
  private RestaurantDao dao;

  @Mock
  private RestaurantRepository repo;

  private Restaurant restaurant;
  private User owner;

  @Before
  public void setup()
  {
    owner = new User();
    owner.setId(ObjectId.get());
    Address address = new Address();
    address.setLabel("Via Altofonte 62a, 90046, Villaciambra");
    restaurant = new Restaurant();
    restaurant.setId(ObjectId.get());
    restaurant.setName("American Pizza");
    restaurant.setDescription("Pizzeria migliore vicino monreale");
    restaurant.setType("pizzeria");
    restaurant.setCuisine("italiana");
    restaurant.setPhoneNumber("0916631196");
    restaurant.setVatNumber("123456789012");
    restaurant.setAddress(address);
    restaurant.setOwner(owner);
    restaurant.setFoodCategories(new HashSet<String>(Arrays.asList(
        "pizze", "antipasti", "bevande", "panini"
    )));


    db.put(restaurant.getId(), restaurant);
  }

  @Test
  public void testUserRestaurants() {
    Mockito.doAnswer((args) -> {
      ObjectId ownerId = args.getArgument(0, ObjectId.class);
      return Optional.of(
          this.db.values().stream()
              .filter((restaurant) -> restaurant.getOwner().getId().equals(ownerId))
              .collect(Collectors.toList())
      );
    }).when(repo).findByOwnerId(owner.getId());

    List<Restaurant> found = this.dao.findAllRestaurantsForUser(owner.getId());
    assertNotNull(found);
    assertEquals(1, found.size());
  }

  @Test
  public void testFindRestaurantById() {
    Mockito.doAnswer((args) -> {

      ObjectId restaurantId = args.getArgument(0, ObjectId.class);
      ObjectId ownerId = args.getArgument(1, ObjectId.class);
      List<Restaurant> founds = this.db.values().stream()
          .filter((restaurant) ->
              restaurant.getId().equals(restaurantId) && restaurant.getOwner().getId().equals(ownerId)
          )
          .collect(Collectors.toList());
      return founds.size() == 1 ? Optional.of(founds.get(0)) : null;
    }).when(repo).findByIdAndOwnerId(restaurant.getId(), owner.getId());

    Restaurant found = this.dao.find(restaurant.getId(), owner.getId());
    assertNotNull(found);
  }

  @Test
  public void testAddNewRestaurant() {
    User proprietary = new User();
    proprietary.setId(ObjectId.get());
    Address address = new Address();
    address.setLabel("Via Zeta Venti, 6, 90046, Villaciambra");
    Restaurant toCreate = new Restaurant();
    toCreate.setId(ObjectId.get());
    toCreate.setName("Pizzeria Da Alessandro");
    toCreate.setDescription("Pizzeria migliore perché è a casa mia");
    toCreate.setType("pizzeria");
    toCreate.setCuisine("italiana");
    toCreate.setPhoneNumber("0916631205");
    toCreate.setVatNumber("123456789012");
    toCreate.setAddress(address);
    toCreate.setOwner(proprietary);
    toCreate.setFoodCategories(new HashSet<String>(Arrays.asList(
        "pizze", "antipasti", "bevande", "panini"
    )));

    WorkingTime monday = new WorkingTime();
    WorkingTime tuesday = new WorkingTime();
    WorkingTime wednesday = new WorkingTime();
    WorkingTime thursday = new WorkingTime();
    WorkingTime friday = new WorkingTime();
    WorkingTime saturday = new WorkingTime();
    WorkingTime sunday = new WorkingTime();

    monday.setFree(true);

    tuesday.setOpening("07:00");
    tuesday.setClosing("23:00");

    wednesday.setOpening("07:00");
    wednesday.setClosing("23:00");

    thursday.setOpening("07:00");
    thursday.setClosing("23:00");

    friday.setOpening("07:00");
    friday.setClosing("23:00");

    saturday.setOpening("07:00");
    saturday.setClosing("23:00");

    sunday.setOpening("07:00");
    sunday.setClosing("23:00");


    TimeTable timeSchedule = new TimeTable();
    timeSchedule.setMonday(monday);
    timeSchedule.setTuesday(tuesday);
    timeSchedule.setWednesday(wednesday);
    timeSchedule.setThursday(thursday);
    timeSchedule.setFriday(friday);
    timeSchedule.setSaturday(saturday);
    timeSchedule.setSunday(sunday);

    toCreate.setWeeklyOpeningSchedule(timeSchedule);

    Mockito.doAnswer((args) -> {
      Restaurant restaurant = args.getArgument(0, Restaurant.class);
      this.db.put(restaurant.getId(), restaurant);
      return restaurant;
    }).when(repo).insert(toCreate);

    dao.create(toCreate);
    assertNotNull(this.db.get(toCreate.getId()));
  }

  @Test
  public void testUpdateRestaurant() {
    Restaurant clone = new Restaurant();
    Address addressClone = new Address();

    addressClone.setLabel(restaurant.getAddress().getLabel());

    clone.setName(restaurant.getName());
    clone.setDescription(restaurant.getDescription());
    clone.setType(restaurant.getType());
    clone.setCuisine(restaurant.getCuisine());
    clone.setId(restaurant.getId());
    clone.setOwner(restaurant.getOwner());
    clone.setFoodCategories(restaurant.getFoodCategories());
    clone.setAddress(addressClone);
    clone.setVatNumber(restaurant.getVatNumber());
    clone.setPhoneNumber(restaurant.getPhoneNumber());

    clone.setName("Old America");
    clone.setDescription("Old america, pizzeria ad altofonte");
    clone.getAddress().setLabel("Viale del fante 50, Altofonte");

    Mockito.doAnswer((args) -> {
      Restaurant restaurant = args.getArgument(0, Restaurant.class);
      this.db.put(restaurant.getId(), restaurant);
      return restaurant;
    }).when(repo).save(clone);

    dao.update(clone);
    assertNotEquals(restaurant.getName(), this.db.get(clone.getId()).getName());
    assertNotEquals(restaurant.getDescription(), this.db.get(clone.getId()).getDescription());
    assertNotEquals(restaurant.getAddress(), this.db.get(clone.getId()).getAddress());
  }

  public void testDeleteRestaurant() {
    Mockito.doAnswer((args) -> {
      Restaurant restaurant = args.getArgument(0, Restaurant.class);
      this.db.remove(restaurant.getId());
      return null;
    }).when(repo).delete(restaurant);

    dao.delete(restaurant);
    assertNull(db.get(restaurant.getId()));
  }

}

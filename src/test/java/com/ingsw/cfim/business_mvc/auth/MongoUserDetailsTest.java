package com.ingsw.cfim.business_mvc.auth;

import static org.junit.Assert.*;

import com.ingsw.cfim.business_mvc.common.models.Role;
import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.profile.models.OwnerProfile;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class MongoUserDetailsTest {

  MongoUserDetails userDetails;

  private SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-DD", Locale.ITALY);

  private User user;

  @Before
  public void setup() throws ParseException {
    List<GrantedAuthority> roles = new ArrayList<>();

    roles.add(new SimpleGrantedAuthority("admin"));

    roles.add(new SimpleGrantedAuthority("student"));

    this.user = new User();
    user.setEmail("example@test.org");
    user.setPassword("testing password");
    user.setRoles(new Role("admin"));
    user.setRoles(new Role("student"));
    user.setEnabled(true);

    OwnerProfile profile = new OwnerProfile();
    profile.setName("test name");
    profile.setLastName("with lastname");
    profile.setCountry("australia");
    profile.setCity("melbourne");
    profile.setDateOfBirth(formatter.parse("1992-12-25"));
    profile.setFiscalNumber("KRNLAE92T25H823O");
    user.setProfile(profile);

    this.userDetails = new MongoUserDetails(this.user, roles);
  }

  @Test
  public void checkCorrectness() {
    assertNotNull(this.userDetails);
    assertEquals("example@test.org", this.userDetails.getUsername());
    assertEquals("testing password", this.userDetails.getPassword());
    assertTrue(this.userDetails.isAccountNonExpired());
    assertTrue(this.userDetails.isAccountNonLocked());
    assertTrue(this.userDetails.isCredentialsNonExpired());
    assertEquals("test name with lastname", this.userDetails.getFullName());
    assertTrue(this.userDetails.isEnabled());

    this.user.getRoles();

    List<String> roles = this.user.getRoles().stream().map((Role r) -> r.getRole()).collect(Collectors.toList());

    List<String> authorities = this.userDetails.getAuthorities().stream().map((GrantedAuthority a) -> a.getAuthority())
        .collect(Collectors.toList());

    assertTrue(roles.containsAll(authorities));
  }

  @Test
  public void testForEmailAsFullNameIfNotProfile() {
    this.user.setProfile(null);
    assertEquals(this.user.getEmail(), this.user.getFullName());
  }

  @Test
  public void thisAuthorityMustNotBePresent() {
    assertFalse(this.userDetails.getAuthorities().contains("restaurant_owner"));
  }

}

package com.ingsw.cfim.business_mvc.common.models;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ProfileTest {
  private Profile profile = new Profile();
  private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);

  @Before()
  public void setup() throws ParseException {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
    this.profile.setName("Mario");
    this.profile.setLastName("Rossi");
    this.profile.setDateOfBirth(formatter.parse("1982-08-11"));
  }

  @Test
  public void verifyThatProfileHasCorrectParameters() {
    assertEquals("Mario", this.profile.getName());
    assertEquals("Rossi", this.profile.getLastName());
    assertEquals("1982-08-11", formatter.format(this.profile.getDateOfBirth()));
    assertEquals("Mario Rossi", this.profile.getFullName());
    assertEquals("August 11, 1982", this.profile.friendlyDob());
  }
}

package com.ingsw.cfim.business_mvc.common.models;

import static org.junit.Assert.*;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;

public class RoleTest {
  private Role role;

  @Before
  public void creatingNewRole() {
    this.role = new Role("student");
    this.role.setId(new ObjectId());
  }

  @Test
  public void creatingRoleWithParamConstructor() {
    Role roleTwo = new Role("admin");
    assertEquals("admin", roleTwo.getRole());
    assertNull(roleTwo.getId());
  }

  @Test
  public void createAndModifyRole() {
    Role role = new Role("restaurant_owner");
    role.setRole("admin");
    assertNotEquals("restaurant_owner", role.getRole());
  }

  @Test
  public void modifyObjectId() {
    ObjectId id = this.role.getId();
    this.role.setId(new ObjectId());
    assertNotEquals(id, this.role.getId());
  }


}

package com.ingsw.cfim.business_mvc.common.models;

import static org.junit.Assert.*;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;

public class UserTest {
  private User user;

  @Before()
  public void setup() {
    this.user = new User("alessandrofrenna95@gmail.com", "testing password");
    this.user.setId(new ObjectId());
    this.user.setEnabled(true);
  }

  @Test
  public void verifyThatUserHasCorrectParameters() {
    assertEquals("alessandrofrenna95@gmail.com", this.user.getEmail());
    assertEquals("testing password", this.user.getPassword());
    assertNotNull(this.user.getRoles());
    assertTrue(this.user.getEnabled());
    assertEquals(0, this.user.getRoles().size());
    assertNotNull(this.user.getId());
  }

  @Test
  public void userRolesMustBeAsListed() {
    Role r_o = new Role("restourant_owner");
    this.user.setRoles(r_o);

    assertFalse(this.user.getRoles().isEmpty());

    Object[] result = this.user.getRoles().stream().filter(
        (Role role) -> role.getRole().equals(r_o.getRole())
    ).toArray();


    assertEquals(1, result.length);

    result = this.user.getRoles().stream().filter(
        (Role role) -> role.getRole().equals("administrator")
    ).toArray();

    assertEquals(0, result.length);
  }
}


package com.ingsw.cfim.business_mvc.products;

import static org.junit.Assert.*;

import com.ingsw.cfim.business_mvc.common.exceptions.ResourceNotFoundException;
import com.ingsw.cfim.business_mvc.products.dao.ProductDao;
import com.ingsw.cfim.business_mvc.products.models.Product;
import com.ingsw.cfim.business_mvc.products.repositories.ProductRepository;
import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DuplicateKeyException;

import java.util.*;
import java.util.stream.Collectors;

@RunWith(MockitoJUnitRunner.class)
public class ProductDaoTest {
  HashMap<ObjectId, Product> db = new HashMap<>();

  @InjectMocks
  private ProductDao dao;

  @Mock
  private ProductRepository repo;

  private Product first;
  private Restaurant restaurant;

  @Before
  public void setup()
  {
    first = new Product("lasagna", 5.50);
    first.setIngredients(Arrays.asList("lasagne", "ragù", "piselli", "besciamella"));
    first.setId(ObjectId.get());
    first.setCategory("primi");

    restaurant = new Restaurant();
    restaurant.setId(ObjectId.get());
    restaurant.setFoodCategories(new HashSet<>(Arrays.asList("primi", "paste", "secondi", "pizze")));
    first.setRestaurant(restaurant);

    db.put(first.getId(), first);
  }

  @Test
  public void testGetAllProductsForRestaurant() {
    Mockito.doAnswer((args) -> {
      ObjectId restaurantId = args.getArgument(0, ObjectId.class);
      return Optional.of(
          this.db.values().stream()
          .filter((product) -> product.getRestaurant().getId().equals(restaurantId))
          .collect(Collectors.toList())
      );
    }).when(repo).findByRestaurantId(restaurant.getId());

    List<Product> found = this.dao.findAllProductsForRestaurant(restaurant.getId());
    assertNotNull(found);
    assertEquals(1, found.size());
  }

  @Test
  public void testGetAllProductsForRestaurantWithCategory() {
    Mockito.doAnswer((args) -> {
      Set<String> categories = args.getArgument(0, Set.class);
      ObjectId restaurantId = args.getArgument(1, ObjectId.class);
      List<Product> products = this.db.values().stream()
          .filter((product) -> product.getRestaurant().getId().equals(restaurantId))
          .collect(Collectors.toList());

      if(products == null) return null;

      List<Product> output = new ArrayList<>();

      categories.forEach(c -> {
        for(Product p: products) {
          if(p.getCategory().equals(c))
            output.add(p);
        }
      });

      return Optional.of(output);
    }).when(repo).findInCategoryAndRestaurantId(restaurant.getFoodCategories(), restaurant.getId());

    List<Product> found = this.dao.findAllProductsForRestaurant(restaurant.getId(), restaurant.getFoodCategories());
    assertNotNull(found);
    assertEquals(1, found.size());
  }

  @Test
  public void testFindProductSuccess() {
    Mockito.doAnswer((args) -> {
      ObjectId productId = args.getArgument(0, ObjectId.class);
      ObjectId restaurantId = args.getArgument(1, ObjectId.class);
      List<Product> founds = this.db.values().stream()
          .filter((product) ->
                  product.getId().equals(productId) && product.getRestaurant().getId().equals(restaurantId)
          )
          .collect(Collectors.toList());
      return founds.size() == 1 ? Optional.of(founds.get(0)) : null;
    }).when(repo).findByIdAndRestaurantId(first.getId(), restaurant.getId());

    var found = this.dao.findProduct(first.getId(), restaurant.getId());
    assertNotNull(found);
    assertEquals(first, found);
  }

  @Test
  public void testAddNewProduct() {
    Product pasta = new Product("maccheroni", 6.00);
    pasta.setId(ObjectId.get());
    pasta.setRestaurant(restaurant);

    Mockito.doAnswer((args) -> {
      Product product = args.getArgument(0, Product.class);
      this.db.put(product.getId(), product);
      return product;
    }).when(repo).insert(pasta);

    dao.createProduct(pasta);
    assertNotNull(this.db.get(pasta.getId()));
  }

  @Test
  public void testUpdateProduct() {
    Product firstClone = new Product();
    firstClone.setName(first.getName());
    firstClone.setIngredients(first.getIngredients());
    firstClone.setPrice(first.getPrice());
    firstClone.setId(first.getId());

    firstClone.setName("pasta al forno");
    firstClone.setIngredients(Arrays.asList("pasta", "ragù", "melenzane", "piselli"));

    Mockito.doAnswer((args) -> {
      Product product = args.getArgument(0, Product.class);
      this.db.put(product.getId(), product);
      return product;
    }).when(repo).save(firstClone);

    dao.updateProduct(firstClone);
    assertNotEquals(first.getName(), this.db.get(firstClone.getId()).getName());
    assertNotEquals(first.getIngredients(), this.db.get(firstClone.getId()).getIngredients());
  }

  public void testDeleteProduct() {
    Mockito.doAnswer((args) -> {
      Product product = args.getArgument(0, Product.class);
      this.db.remove(product.getId());
      return null;
    }).when(repo).delete(first);

    dao.deleteProduct(first);
    assertNull(db.get(first.getId()));
  }

}
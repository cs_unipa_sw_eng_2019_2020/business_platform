package com.ingsw.cfim.business_mvc.profile.builders;

import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.profile.models.OwnerProfile;
import com.ingsw.cfim.business_mvc.registration.RegistrationService;
import com.ingsw.cfim.business_mvc.registration.forms.RegistrationForm;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class OwnerBuilderTest {

  @Mock
  RegistrationService svc;

  OwnerProfile profile = new OwnerProfile();

  User user = new User();

  @Before
  public void setup() throws ParseException {

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);

    user.setEmail("mariorossi@rossi.it");
    user.setPassword("test");

    profile.setName("Mario");
    profile.setLastName("Rossi");
    profile.setDateOfBirth(formatter.parse("1982-12-22"));
    profile.setCountry("Italy");
    profile.setCity("Roma");
    profile.setFiscalNumber("RSSMRA82H11G273K");

    user.setProfile(profile);

    Mockito.when(
        this.svc.register(Mockito.any())
    ).thenReturn(user);
  }

  @Test
  public void testCreateUserWithProfile() throws ParseException {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);

    OwnerProfile p = new OwnerProfile();

    p.setName("Mario");
    p.setLastName("Rossi");
    p.setDateOfBirth(formatter.parse("1982-12-22"));
    p.setCountry("Italy");
    p.setCity("Roma");
    p.setFiscalNumber("RSSMRA82H11G273K");

    RegistrationForm form = new RegistrationForm();
    form.setEmail("mariorossi@rossi.it");
    form.setPassword("test");

    User created = OwnerBuilder.init(svc)
        .user(form)
        .profile(p)
        .build();

    assertNotNull(created);
  }

}

const monday = document.querySelector("input[id=monday]");
const tuesday = document.querySelector("input[id=tuesday]");
const wednesday = document.querySelector("input[id=wednesday]");
const thursday = document.querySelector("input[id=thursday]");
const friday = document.querySelector("input[id=friday]");
const saturday = document.querySelector("input[id=saturday]");
const sunday = document.querySelector("input[id=sunday]");

monday.addEventListener("change", (event) => {
    const opening = document.querySelector("#monday_opening");
    const closing = document.querySelector("#monday_closing");
    if(monday.checked) {
        opening.setAttribute("disabled", "disabled");
        opening.setAttribute("value", "")
        closing.setAttribute("disabled", "disabled");
        closing.setAttribute("value", "")
        opening.removeAttribute("required");
        closing.removeAttribute("required");
    } else {
        opening.removeAttribute("disabled");
        closing.removeAttribute("disabled");
        opening.setAttribute("required", "required");
        closing.setAttribute("required", "required");
    }
});

tuesday.addEventListener("change", (event) => {
    const opening = document.querySelector("#tuesday_opening");
    const closing = document.querySelector("#tuesday_closing");
    if(tuesday.checked) {
        opening.setAttribute("disabled", "disabled");
        opening.setAttribute("value", "")
        closing.setAttribute("disabled", "disabled");
        closing.setAttribute("value", "")
        opening.removeAttribute("required");
        closing.removeAttribute("required");
    } else {
        opening.removeAttribute("disabled");
        closing.removeAttribute("disabled");
        opening.setAttribute("required", "required");
        closing.setAttribute("required", "required");
    }
});

wednesday.addEventListener("change", (event) => {
    const opening = document.querySelector("#wednesday_opening");
    const closing = document.querySelector("#wednesday_closing");
    if(wednesday.checked) {
        opening.setAttribute("disabled", "disabled");
        opening.setAttribute("value", "")
        closing.setAttribute("disabled", "disabled");
        closing.setAttribute("value", "")
        opening.removeAttribute("required");
        closing.removeAttribute("required");
    } else {
        opening.removeAttribute("disabled");
        closing.removeAttribute("disabled");
        opening.setAttribute("required", "required");
        closing.setAttribute("required", "required");
    }
});

thursday.addEventListener("change", (event) => {
    const opening = document.querySelector("#thursday_opening");
    const closing = document.querySelector("#thursday_closing");
    if(thursday.checked) {
        opening.setAttribute("disabled", "disabled");
        opening.setAttribute("value", "")
        closing.setAttribute("disabled", "disabled");
        closing.setAttribute("value", "")
        opening.removeAttribute("required");
        closing.removeAttribute("required");
    } else {
        opening.removeAttribute("disabled");
        closing.removeAttribute("disabled");
        opening.setAttribute("required", "required");
        closing.setAttribute("required", "required");
    }
});

friday.addEventListener("change", (event) => {
    const opening = document.querySelector("#friday_opening");
    const closing = document.querySelector("#friday_closing");
    if(friday.checked) {
        opening.setAttribute("disabled", "disabled");
        opening.setAttribute("value", "")
        closing.setAttribute("disabled", "disabled");
        closing.setAttribute("value", "")
        opening.removeAttribute("required");
        closing.removeAttribute("required");
    } else {
        opening.removeAttribute("disabled");
        closing.removeAttribute("disabled");
        opening.setAttribute("required", "required");
        closing.setAttribute("required", "required");
    }
});

saturday.addEventListener("change", (event) => {
    const opening = document.querySelector("#saturday_opening");
    const closing = document.querySelector("#saturday_closing");
    if(saturday.checked) {
        opening.setAttribute("disabled", "disabled");
        opening.setAttribute("value", "")
        closing.setAttribute("disabled", "disabled");
        closing.setAttribute("value", "")
        opening.removeAttribute("required");
        closing.removeAttribute("required");
    } else {
        opening.removeAttribute("disabled");
        closing.removeAttribute("disabled");
        opening.setAttribute("required", "required");
        closing.setAttribute("required", "required");
    }
});

sunday.addEventListener("change", (event) => {
    const opening = document.querySelector("#sunday_opening");
    const closing = document.querySelector("#sunday_closing");
    if(sunday.checked) {
        opening.setAttribute("disabled", "disabled");
        opening.setAttribute("value", "")
        closing.setAttribute("disabled", "disabled");
        closing.setAttribute("value", "")
    } else {
        opening.removeAttribute("disabled");
        closing.removeAttribute("disabled");
        opening.setAttribute("required", "required");
        closing.setAttribute("required", "required");
    }
});

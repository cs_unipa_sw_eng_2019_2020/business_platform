package com.ingsw.cfim.business_mvc.auth;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
  @GetMapping("/login")
  public String showLoginForm(Model model) {
    return "pages/login/index";
  }

  @RequestMapping("/login_error")
  public String addLoginErrors(Model model) {
    model.addAttribute("login_error", "Invalid username or password");
    return "pages/login/index";
  }
}

package com.ingsw.cfim.business_mvc.auth;

import com.ingsw.cfim.business_mvc.common.models.Role;
import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.common.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AuthService implements UserDetailsService {

  @Autowired
  private UserRepository userRepo;

  @Autowired
  private CurrentLoggedIn current;

  protected List<GrantedAuthority> getUserAuthority(Set<Role> userRoles) {
    Set<GrantedAuthority> roles = new HashSet<>();
    userRoles.forEach((role) -> {
      roles.add(new SimpleGrantedAuthority(role.getRole()));
    });
    return new ArrayList<>(roles);
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    User user = this.userRepo.findUserByEmail(email);
    if (user != null) {
      current.setUser(user);
      List<GrantedAuthority> authorities = this.getUserAuthority(user.getRoles());
      return new MongoUserDetails(user, authorities);
    } else {
      throw new UsernameNotFoundException("User with given email not found");
    }
  }

}

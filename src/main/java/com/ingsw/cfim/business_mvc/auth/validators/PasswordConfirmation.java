package com.ingsw.cfim.business_mvc.auth.validators;
import javax.validation.Constraint;

import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PasswordConfirmationConstraint.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordConfirmation {
  String message() default "Password Confirmation must be equals to password";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
}



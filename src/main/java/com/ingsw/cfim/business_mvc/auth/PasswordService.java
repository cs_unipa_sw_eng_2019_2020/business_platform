package com.ingsw.cfim.business_mvc.auth;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService implements PasswordEncoder {
  @Override
  public String encode(CharSequence plainText) {
    return BCrypt.hashpw(plainText.toString(), BCrypt.gensalt(10));
  }

  @Override
  public boolean matches(CharSequence plainText, String hashed) {
    return BCrypt.checkpw(plainText.toString(), hashed);
  }
}


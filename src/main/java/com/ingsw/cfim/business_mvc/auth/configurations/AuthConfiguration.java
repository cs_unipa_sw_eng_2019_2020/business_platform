package com.ingsw.cfim.business_mvc.auth.configurations;

import com.ingsw.cfim.business_mvc.auth.PasswordService;
import com.ingsw.cfim.business_mvc.auth.AuthService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class AuthConfiguration extends WebSecurityConfigurerAdapter {

  @Bean
  public AuthService getAuthService() {
    return new AuthService();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new PasswordService();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth
        .userDetailsService(this.getAuthService())
        .passwordEncoder(this.passwordEncoder());
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http
        .csrf().disable()
        .authorizeRequests()
        .antMatchers("/auth/**").permitAll()
        .antMatchers("/products/**").permitAll()
        .antMatchers("/restaurants/**").hasAuthority("restaurant_owner")
        .antMatchers("/profile/**").hasAuthority("restaurant_owner")
        .and()
        .formLogin()
        .loginPage("/login")
        .failureUrl("/login_error")
        .permitAll()
        .defaultSuccessUrl("/")
        .and()
        .logout()
        .invalidateHttpSession(true)
        .clearAuthentication(true)
        .deleteCookies();
  }

}
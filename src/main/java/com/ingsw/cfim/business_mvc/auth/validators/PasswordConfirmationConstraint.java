package com.ingsw.cfim.business_mvc.auth.validators;

import com.ingsw.cfim.business_mvc.registration.forms.RegistrationForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordConfirmationConstraint implements ConstraintValidator<PasswordConfirmation, RegistrationForm> {
  @Override
  public void initialize(PasswordConfirmation passwordConfirmation) {
  }

  @Override
  public boolean isValid(RegistrationForm form, ConstraintValidatorContext cxt) {
    if(form.getPassword() == null && form.getPasswordConfirmation() == null) return true;
    if(form.getPassword() == null) return false;
    if(form.getPassword().equals(form.getPasswordConfirmation())) return true;

    return false;
  }
}

package com.ingsw.cfim.business_mvc.auth;

import com.ingsw.cfim.business_mvc.common.models.User;
import org.springframework.stereotype.Service;

@Service
public class CurrentLoggedIn {

  private User current;

  public void setUser(User user) {
    this.current = user;
  }

  public User getUser() {
    return current;
  }
}

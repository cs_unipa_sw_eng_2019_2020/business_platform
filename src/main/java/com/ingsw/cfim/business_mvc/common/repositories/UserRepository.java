package com.ingsw.cfim.business_mvc.common.repositories;

import com.ingsw.cfim.business_mvc.common.models.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, ObjectId> {
  public User findUserByEmail(String email);
}

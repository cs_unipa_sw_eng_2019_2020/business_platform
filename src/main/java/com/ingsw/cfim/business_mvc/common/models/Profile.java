package com.ingsw.cfim.business_mvc.common.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.beans.Transient;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Profile {
  @NotBlank(message = "Name must not be blank")
  private String name;

  @NotBlank(message = "Last name must not be blank")
  private String lastName;

  @NotNull(message="Date of birth must not be null")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private Date dateOfBirth;

  @Transient
  public String getFullName() {
    return this.name + " " + this.lastName;
  }

  @Transient
  public String friendlyDob() {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    String date = formatter.format(this.dateOfBirth);
    return LocalDate.parse(date).format(
        DateTimeFormatter.ofLocalizedDate(
            FormatStyle.LONG
        )
    );
  }
}

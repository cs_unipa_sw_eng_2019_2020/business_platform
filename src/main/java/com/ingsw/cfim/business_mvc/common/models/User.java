package com.ingsw.cfim.business_mvc.common.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.HashSet;
import java.util.Set;

@Data
@EqualsAndHashCode(of = "id")
@Document(collection = "users")
public class User {
  @MongoId
  private ObjectId id;

  @Indexed(name = "email_index", unique = true, direction = IndexDirection.DESCENDING)
  private String email;
  private String password;
  private Boolean enabled;

  @DBRef
  private Set<Role> roles;

  private Profile profile;

  public User() {
    this.roles = new HashSet<Role>();
  }

  @PersistenceConstructor
  public User(String email, String password) {
    this();
    this.email = email;
    this.password = password;
  }

  @Transient
  public String getFullName() {
    if (this.profile == null)
      return this.email;
    return this.profile.getFullName();
  }

  public void setRoles(Role role) {
    this.roles.add(role);
  }
}

package com.ingsw.cfim.business_mvc.restaurants.responses;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import com.ingsw.cfim.business_mvc.restaurants.models.TimeTable;
import com.ingsw.cfim.business_mvc.restaurants.models.WorkingTime;
import lombok.EqualsAndHashCode;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;

@EqualsAndHashCode(callSuper = false)
public class RestaurantInfo {
  @JsonIgnore
  private final Restaurant restaurant;

  public RestaurantInfo(Restaurant restaurant) {
    this.restaurant = restaurant;
  }

  @JsonGetter("id")
  public String getId() {
    return restaurant.getId().toString();
  }

  @JsonGetter("description")
  public String getDescription() {
    return restaurant.getDescription();
  }

  @JsonGetter("name")
  public String getName() {
    return restaurant.getName();
  }

  @JsonGetter("type")
  public String getType() {
    return restaurant.getType();
  }

  @JsonGetter("phoneNumber")
  public String getPhoneNumber() {
    return restaurant.getPhoneNumber();
  }

  @JsonGetter("address")
  public String getAddress() {
    return restaurant.getAddress().getLabel();
  }

  @JsonGetter("openingStatus")
  public String getOpeningStatus() throws ParseException {
    String today = LocalDateTime.now().getDayOfWeek().toString();
    TimeTable schedule = this.restaurant.getWeeklyOpeningSchedule();
    WorkingTime todaySchedule = null;

    if (schedule == null) {
      return null;
    }

    switch (today) {
    case "MONDAY":
      todaySchedule = schedule.getMonday();
      break;
    case "TUESDAY":
      todaySchedule = schedule.getTuesday();
      break;
    case "WEDNESDAY":
      todaySchedule = schedule.getWednesday();
      break;
    case "THURSDAY":
      todaySchedule = schedule.getThursday();
      break;
    case "FRIDAY":
      todaySchedule = schedule.getFriday();
      break;
    case "SATURDAY":
      todaySchedule = schedule.getSaturday();
      break;
    case "SUNDAY":
      todaySchedule = schedule.getSunday();
      break;
    default:
      return "unknown";
    }

    if (todaySchedule.getFree()) {
      return "dayOff";
    }

    LocalTime openingTime = LocalTime.parse(todaySchedule.getOpening());
    LocalTime closingTime = LocalTime.parse(todaySchedule.getClosing());

    LocalDateTime opening = LocalDateTime.now().withHour(openingTime.getHour()).withMinute(openingTime.getMinute());
    LocalDateTime closing = LocalDateTime.now().withHour(closingTime.getHour()).withMinute(closingTime.getMinute());
    LocalDateTime now = LocalDateTime.now();

    if (closing.isBefore(opening)) {
      closing = closing.plusHours(24);
    }

    if (opening.isBefore(now) && closing.isAfter(now)) {
      return "open";
    }

    return "closed";
  }
}

package com.ingsw.cfim.business_mvc.restaurants.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ingsw.cfim.business_mvc.auth.CurrentLoggedIn;
import com.ingsw.cfim.business_mvc.common.exceptions.ResourceNotFoundException;
import com.ingsw.cfim.business_mvc.geo.GeocodeRequest;
import com.ingsw.cfim.business_mvc.products.dao.IProductDao;
import com.ingsw.cfim.business_mvc.products.forms.ProductForm;
import com.ingsw.cfim.business_mvc.products.models.Product;
import com.ingsw.cfim.business_mvc.restaurants.dao.IRestaurantDao;
import com.ingsw.cfim.business_mvc.restaurants.forms.RestaurantForm;
import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import com.ingsw.cfim.business_mvc.restaurants.models.TimeTable;
import com.mongodb.MongoWriteException;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class RestaurantsService implements IRestaurantsService {

  private IRestaurantDao restaurantsDao;
  private IProductDao productDao;
  private CurrentLoggedIn current;
  private GeocodeRequest geo;

  public RestaurantsService(
      IRestaurantDao restaurants, IProductDao products, CurrentLoggedIn current, ApplicationContext ctx
  ) {
    this.restaurantsDao = restaurants;
    this.productDao = products;
    this.current = current;
    this.geo = ctx.getBean(GeocodeRequest.class);
  }

  @Override
  public Restaurant getRestaurant(ObjectId restaurantId) {
    String message = String.format("Resource with give id, %s not found, cannot update", restaurantId);

    return Optional.ofNullable(
        restaurantsDao.find(restaurantId, current.getUser().getId())
    ).orElseThrow(
        () -> new ResourceNotFoundException(message)
    );
  }

  @Override
  public List<Restaurant> getAllRestaurants() {
    return restaurantsDao.findAllRestaurantsForUser(current.getUser().getId());
  }

  @Override
  public void createRestaurant(RestaurantForm input) throws MongoWriteException, JsonProcessingException {
    Restaurant restaurant = parseRestaurantFormToRestaurant(input);
    this.restaurantsDao.create(restaurant);
  }

  @Override
  public void updateRestaurant(ObjectId id, RestaurantForm input) throws JsonProcessingException, ResourceNotFoundException {
    Restaurant toUpdate = getRestaurant(id);

    if(toUpdate == null) {
      String message = String.format("Restaurant with id %s not found ", id);
      throw new ResourceNotFoundException(message);
    }

    ObjectId restaurantID = toUpdate.getId();

    Set<String> originalCategories = toUpdate.getFoodCategories();
    Set<String> updatedCategories = input.getFoodCategories();
    originalCategories.removeIf(updatedCategories::contains);

    if(originalCategories.size() > 0 && hasProductsOfGivenCategories(toUpdate, originalCategories)) {
      log.error("Cannot update restaurant {} because products with modified categories {} exist", toUpdate.getId(), originalCategories);
      throw new IllegalStateException("Cannot update restaurants because exists products with modified categories.");
    }

    TimeTable workingSchedule = toUpdate.getWeeklyOpeningSchedule();

    toUpdate = parseRestaurantFormToRestaurant(input);
    toUpdate.setWeeklyOpeningSchedule(workingSchedule);
    toUpdate.setId(restaurantID);
    this.restaurantsDao.update(toUpdate);
  }

  @Override
  public void deleteRestaurant(ObjectId restaurantID) throws ResourceNotFoundException {
    Restaurant toDelete = getRestaurant(restaurantID);

    if(toDelete == null) {
      String message = String.format("Restaurant with id %s not found ", restaurantID);
      throw new ResourceNotFoundException(message);
    }

    List<Product> products = productDao.findAllProductsForRestaurant(restaurantID);
    products.forEach(p -> productDao.deleteProduct(p));
    restaurantsDao.delete(toDelete);
  }

  @Override
  public void updateWorkingTimeScheduleForRestaurant(ObjectId restaurantId, TimeTable schedule) {
    Restaurant toUpdate = getRestaurant(restaurantId);

    if(toUpdate == null) {
      String message = String.format("Restaurant with id %s not found ", restaurantId);
      throw new ResourceNotFoundException(message);
    }

    toUpdate.setWeeklyOpeningSchedule(schedule);
    restaurantsDao.update(toUpdate);
  }

  @Override
  public List<Product> getRestaurantProducts(ObjectId restaurantId) {
    return this.productDao.findAllProductsForRestaurant(restaurantId);
  }

  @Override
  public void addProduct(ObjectId restaurantId, ProductForm input) {
    Restaurant restaurant = getRestaurant(restaurantId);

    Product toCreate = new Product();

    if(!restaurant.getFoodCategories().contains(input.getCategory())) {
      restaurant.addCategory(input.getCategory());
      restaurantsDao.update(restaurant);
    }

    toCreate.setName(input.getName());
    toCreate.setCategory(input.getCategory());
    toCreate.setIngredients(input.getIngredients());
    toCreate.setPrice(input.getPrice());
    toCreate.setRestaurant(restaurant);
    productDao.createProduct(toCreate);
  }

  @Override
  public void updateProduct(ObjectId restaurantId, ObjectId productId, ProductForm input) {
    Product toUpdate = productDao.findProduct(productId, restaurantId);

    if(toUpdate == null) {
      String message = String.format("Product with id %s not found for restaurant with id %s", productId.toString(), restaurantId.toString());
      throw new ResourceNotFoundException(message);
    }

    Restaurant restaurant = toUpdate.getRestaurant();

    if(toUpdate.getCategory() == null || !toUpdate.getCategory().equals(input.getCategory())) {
      if(!restaurant.getFoodCategories().contains(input.getCategory())) {
        restaurant.addCategory(input.getCategory());
        restaurantsDao.update(restaurant);
      }
    }

    toUpdate.setName(input.getName());
    toUpdate.setCategory(input.getCategory());
    toUpdate.setPrice(input.getPrice());
    toUpdate.setIngredients(input.getIngredients());
    toUpdate.setRestaurant(restaurant);
    productDao.updateProduct(toUpdate);
  }

  @Override
  public void deleteProductFromRestaurant(ObjectId productId, ObjectId restaurantId) {
    Product toDelete = productDao.findProduct(productId, restaurantId);
    if(toDelete == null) {
      String message = String.format("Product with id %s not found for restaurant with id %s", productId.toString(), restaurantId.toString());
      throw new ResourceNotFoundException(message);
    }
    productDao.deleteProduct(toDelete);
  }

  @Override
  public Restaurant parseRestaurantFormToRestaurant(RestaurantForm form) throws JsonProcessingException {
    Restaurant restaurant = new Restaurant();

    restaurant.setName(form.getName());
    restaurant.setDescription(form.getDescription());
    restaurant.setCuisine(form.getCuisine());
    restaurant.setType(form.getType());
    restaurant.setFoodCategories(form.getFoodCategories());
    restaurant.setPhoneNumber(form.getPhoneNumber());
    restaurant.setVatNumber(form.getVatNumber());
    restaurant.setAddress(geo.getAddressFromAPI(form.getAddress()));
    restaurant.setOwner(current.getUser());

    return restaurant;
  }

  @Override
  public RestaurantForm parseRestaurantToRestaurantForm(ObjectId restaurantID) throws ResponseStatusException {
    Restaurant toParse = getRestaurant(restaurantID);
    RestaurantForm form = new RestaurantForm();
    form.setName(toParse.getName());
    form.setDescription(toParse.getDescription());
    form.setCuisine(toParse.getCuisine());
    form.setType(toParse.getType());
    form.setFoodCategories(toParse.getFoodCategories());
    form.setPhoneNumber(toParse.getPhoneNumber());
    form.setVatNumber(toParse.getVatNumber());
    form.setAddress(toParse.getAddress().getLabel());
    return form;
  }

  @Override
  public ProductForm parseProductToProductForm(ObjectId productId, ObjectId restaurantId) {
    Product found = productDao.findProduct(productId, restaurantId);
    ProductForm form = new ProductForm();
    form.setName(found.getName());
    form.setCategory(found.getCategory());
    form.setPrice(found.getPrice());
    form.setIngredients(found.getIngredients());
    return form;
  }

  @Override
  public Boolean hasProductsOfGivenCategories(Restaurant restaurant, Set<String> categories) {
    List<Product> products = productDao.findAllProductsForRestaurant(restaurant.getId(), categories);
    return products.size() > 0;
  }

}

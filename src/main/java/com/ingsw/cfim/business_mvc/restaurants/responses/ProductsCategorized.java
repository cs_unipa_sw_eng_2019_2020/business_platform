package com.ingsw.cfim.business_mvc.restaurants.responses;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.ingsw.cfim.business_mvc.products.models.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProductsCategorized {
  private final List<Product> products;

  public ProductsCategorized(final List<Product> products) {
    this.products = products;
  }

  @JsonGetter("products")
  public Map<String, List<ProductInfo>> getProducts() {
    if (this.products == null)
      return null;

    Map<String, List<ProductInfo>> categorized = new HashMap<>();

    List<ProductInfo> products = this.products.stream().map(ProductInfo::new).collect(Collectors.toList());

    products.forEach(p -> {
      List<ProductInfo> pList;
      if (!categorized.containsKey(p.getCategory())) {
        pList = new ArrayList<>();
      } else {
        pList = categorized.get(p.getCategory());
      }
      pList.add(p);
      categorized.put(p.getCategory(), pList);
    });

    return categorized;
  }
}

package com.ingsw.cfim.business_mvc.restaurants.responses;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ingsw.cfim.business_mvc.products.models.Product;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = false)
@JsonPropertyOrder({"id", "name", "category", "ingredients", "price"})
public class ProductInfo {
  @JsonIgnore
  private final Product product;

  public ProductInfo(Product product) {
    this.product = product;
  }

  @JsonGetter("id")
  public String getId() {
    return product.getId().toString();
  }

  @JsonGetter("category")
  public String getCategory() {
    return product.getCategory();
  }

  @JsonGetter("name")
  public String getName() {
    return product.getName();
  }

  @JsonGetter("ingredients")
  public String getIngredients() {
    return String.join(", ", product.getIngredients());
  }

  @JsonGetter("price")
  public Double getPrice() {
    return product.getPrice();
  }
}

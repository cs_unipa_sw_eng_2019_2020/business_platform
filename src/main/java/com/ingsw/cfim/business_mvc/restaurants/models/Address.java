package com.ingsw.cfim.business_mvc.restaurants.models;

import lombok.Data;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;

@Data
public class Address {
  private String label;
  private String country;
  private String state;
  private String county;
  private String city;
  private String district;
  private String street;
  private String number;
  private Integer postalCode;

  @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
  private GeoJsonPoint location;
}

package com.ingsw.cfim.business_mvc;

import com.ingsw.cfim.business_mvc.common.models.Role;
import com.ingsw.cfim.business_mvc.common.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;
import java.util.TimeZone;

@SpringBootApplication
@EnableMongoAuditing
public class BusinessMvcApplication {
	@Autowired
	RoleRepository roleRepo;

	public static void main(String[] args) {
		SpringApplication.run(BusinessMvcApplication.class, args);
	}

	@Bean
	public LocaleResolver setLocale() {
		SessionLocaleResolver locale = new SessionLocaleResolver();
		locale.setDefaultLocale(Locale.ITALY);
		locale.setDefaultTimeZone(TimeZone.getTimeZone("ECT"));
		return locale;
	}

	@Bean()
	public void dataLoader() {
		if(this.roleRepo.findByRole("restaurant_owner") == null) {
			this.roleRepo.save(new Role("restaurant_owner"));
		}
	}
}

package com.ingsw.cfim.business_mvc.profile.models;

import com.ingsw.cfim.business_mvc.common.models.Profile;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class OwnerProfile extends Profile {
  @NotBlank(message = "Fiscal code must not be blank")
  @Size(min = 16, max = 16, message = "Fiscal code length must be 16 chars")
  @Pattern(regexp = "^[A-Z]{6}[0-9]{2}[A|B|C|D|E|H|L|M|P|R|S|T][0-9]{2}\\w{4}[A-Z]$", message = "Fiscal code is invalid")
  private String fiscalNumber;

  @NotBlank(message = "Country must not be blank")
  private String country;

  @NotBlank(message = "City must not be blank")
  private String city;

  public OwnerProfile() {
    super();
  }
}

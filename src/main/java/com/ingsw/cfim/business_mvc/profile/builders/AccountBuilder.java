package com.ingsw.cfim.business_mvc.profile.builders;

import com.ingsw.cfim.business_mvc.common.models.Profile;
import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.registration.IRegistrationService;
import com.ingsw.cfim.business_mvc.registration.forms.RegistrationForm;
import org.springframework.dao.DuplicateKeyException;

public interface AccountBuilder {
  public static AccountBuilder init(IRegistrationService svc) {
    return null;
  }
  public AccountBuilder user(RegistrationForm userInput);
  public AccountBuilder profile(Profile profileInput);
  public User build() throws DuplicateKeyException;
}

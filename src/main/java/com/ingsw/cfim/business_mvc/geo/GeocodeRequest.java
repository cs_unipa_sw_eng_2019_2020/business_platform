package com.ingsw.cfim.business_mvc.geo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ingsw.cfim.business_mvc.restaurants.models.Address;
import com.ingsw.cfim.business_mvc.geo.builder.Geocode;
import com.ingsw.cfim.business_mvc.geo.parsers.AddressParser;
import com.ingsw.cfim.business_mvc.geo.parsers.GeoResponseParser;
import com.ingsw.cfim.business_mvc.geo.services.GeoAPI;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

public class GeocodeRequest {

  @Autowired
  private GeoAPI geoAPI;

  private Geocode builder;
  private String endpoint;
  private String apiKey;

  public GeocodeRequest(Geocode geocodeBuilder, String geoEndpoint, String apiKey) {
    this.builder = geocodeBuilder;
    this.endpoint = geoEndpoint;
    this.apiKey = apiKey;
  }

  public Address getAddressFromAPI(String address) throws JsonProcessingException {
    String url = builder
        .setAPIEndpoint(this.endpoint)
        .setRequestParam("apiKey", this.apiKey)
        .setRequestParam("searchText", address)
        .buildRequestUrl();

    JSONObject json = this.geoAPI.getCoordinatesFromAddress(url);

    if(json == null) {
      return null;
    }

    GeoResponseParser parser = new AddressParser(json);
    return parser
        .parseAddress()
        .parseLocation()
        .getParsedAddress();
  }

}

package com.ingsw.cfim.business_mvc.geo.services;

import org.json.JSONObject;

public interface GeoAPI {
  public JSONObject getCoordinatesFromAddress(String address);
}

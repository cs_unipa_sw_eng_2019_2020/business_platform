package com.ingsw.cfim.business_mvc.registration;

import com.ingsw.cfim.business_mvc.common.models.User;

public interface IRegistrationService {
  User register(User user);
  String hashPassword(String plainText);
}

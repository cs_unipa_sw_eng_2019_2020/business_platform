package com.ingsw.cfim.business_mvc.registration.forms;

import com.ingsw.cfim.business_mvc.auth.validators.PasswordConfirmation;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@PasswordConfirmation
public class RegistrationForm {
  @NotBlank(message = "Email must not be blank")
  @Email(message = "Email must be a valid email")
  private String email;

  @NotBlank(message = "Password must not be blank")
  private String password;

  private String passwordConfirmation;
}

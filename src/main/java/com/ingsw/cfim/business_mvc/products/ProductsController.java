package com.ingsw.cfim.business_mvc.products;

import com.ingsw.cfim.business_mvc.common.exceptions.ResourceNotFoundException;
import com.ingsw.cfim.business_mvc.products.forms.ProductForm;
import com.ingsw.cfim.business_mvc.restaurants.responses.ProductInfo;
import com.ingsw.cfim.business_mvc.restaurants.services.IRestaurantsService;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class ProductsController {
  @Autowired
  private IRestaurantsService restaurantsService;

  @GetMapping("/restaurants/{id}/products")
  public String showAllProducts(@PathVariable("id") ObjectId id, Model model) {
    List<ProductInfo> products = restaurantsService.getRestaurantProducts(id).stream().map(ProductInfo::new)
        .collect(Collectors.toList());
    model.addAttribute("r_id", id);
    model.addAttribute("products", products);
    return "pages/products/index";
  }

  @GetMapping("/restaurants/{id}/products/create")
  public String showCreateProductForm(@PathVariable("id") ObjectId id, ProductForm productForm, Model model) {
    model.addAttribute("add", true);
    model.addAttribute("r_id", id);
    return "pages/products/add-edit";
  }

  @PostMapping(value = "/restaurants/{id}/products/create")
  public String createProduct(@PathVariable("id") ObjectId id, @Valid ProductForm productForm, BindingResult result,
      RedirectAttributes redirect, Model model) {
    if (result.hasErrors()) {
      model.addAttribute("add", true);
      model.addAttribute("r_id", id);
      return "pages/products/add-edit";
    }

    String path = String.format("redirect:/restaurants/%s/products", id.toString());

    try {
      this.restaurantsService.addProduct(id, productForm);
      return path;
    } catch (DuplicateKeyException exception) {
      log.error("Product with name {} already exists for restaurant with id {}", productForm.getName(), id.toString());
      redirect.addFlashAttribute("creation_error", "Il prodotto non è stato creato poiché già esistente");
      return path;
    }
  }

  @GetMapping("/restaurants/{restaurantId}/products/{productId}/update")
  public String showUpdateProductForm(ProductForm productForm, @PathVariable("restaurantId") ObjectId restaurantId,
      @PathVariable("productId") ObjectId productId, Model model) {
    model.addAttribute("add", false);
    model.addAttribute("r_id", restaurantId);
    model.addAttribute("p_id", productId);

    try {
      model.addAttribute("productForm", restaurantsService.parseProductToProductForm(productId, restaurantId));
      return "pages/products/add-edit";
    } catch (ResourceNotFoundException exception) {
      log.error(exception.getMessage());
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/restaurants/{restaurantId}/products/{productId}/update")
  public String updateProduct(@Valid ProductForm productForm, @PathVariable("restaurantId") ObjectId restaurantId,
      @PathVariable("productId") ObjectId productId, BindingResult result, RedirectAttributes redirect, Model model) {
    if (result.hasErrors()) {
      model.addAttribute("add", false);
      model.addAttribute("r_id", restaurantId);
      model.addAttribute("p_id", productId);
      return "pages/products/add-edit";
    }

    String path = String.format("redirect:/restaurants/%s/products", restaurantId.toString());

    try {
      restaurantsService.updateProduct(restaurantId, productId, productForm);
      return path;
    } catch (ResourceNotFoundException exception) {
      log.error(exception.getMessage());
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    } catch (Exception exception) {
      log.error("Cannot update product {} because an error occurred {}", productForm.getName(), exception.getMessage());
      redirect.addFlashAttribute("creation_error", "Non è stato possibile aggiornare il prodotto");
      return path;
    }
  }

  @GetMapping("/restaurants/{rId}/products/{pId}/delete")
  public String deleteProduct(@PathVariable("rId") ObjectId rId, @PathVariable("pId") ObjectId pId, Model model) {
    try {
      this.restaurantsService.deleteProductFromRestaurant(pId, rId);
      String path = String.format("redirect:/restaurants/%s/products", rId.toString());
      return path;
    } catch (ResourceNotFoundException exception) {
      log.error(exception.getMessage());
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }
}

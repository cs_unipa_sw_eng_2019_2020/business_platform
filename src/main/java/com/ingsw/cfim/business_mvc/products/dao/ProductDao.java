package com.ingsw.cfim.business_mvc.products.dao;

import com.ingsw.cfim.business_mvc.products.models.Product;
import com.ingsw.cfim.business_mvc.products.repositories.ProductRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ProductDao implements IProductDao {

  @Autowired
  private ProductRepository products;

  @Override
  public List<Product> findAllProductsForRestaurant(final ObjectId restaurantId) {
    return products.findByRestaurantId(restaurantId).orElse(null);
  }

  @Override
  public List<Product> findAllProductsForRestaurant(final ObjectId restaurantId, final Set<String> categories) {
    return products.findInCategoryAndRestaurantId(categories, restaurantId).orElse(null);
  }

  @Override
  public Product findProduct(final ObjectId productId, final ObjectId restaurantId) {
    return products.findByIdAndRestaurantId(productId, restaurantId).orElse(null);
  }

  @Override
  public void createProduct(final Product product) {
    products.insert(product);
  }

  @Override
  public void updateProduct(final Product product) {
    products.save(product);
  }

  @Override
  public void deleteProduct(final Product product) {
    products.delete(product);
  }
}
